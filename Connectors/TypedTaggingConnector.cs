﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Factories;
using TypedTaggingModule.Models;
using TypedTaggingModule.Services;

namespace TypedTaggingModule.Connectors
{
    public class TypedTaggingConnector : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private ServiceAgentElastic serviceAgentElastic;

        private object connectorLocker = new object();

        private TypedTaggingFactory typedTaggingFactory;

        private ConnectorResultTypedTags resultSaveTypedTags;
        public ConnectorResultTypedTags ResultSaveTypedTags
        {
            get
            {
                lock(connectorLocker)
                {
                    return resultSaveTypedTags;
                }
                
            }
            set
            {
                lock (connectorLocker)
                {
                    resultSaveTypedTags = value;
                }
                RaisePropertyChanged(nameof(ResultSaveTypedTags));
            }
        }

        private ConnectorResultTypedTags resultSaveTypedTagsNoView;
        public ConnectorResultTypedTags ResultSaveTypedTagsNoView
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultSaveTypedTagsNoView;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultSaveTypedTagsNoView = value;
                }
                RaisePropertyChanged(nameof(ResultSaveTypedTagsNoView));
            }
        }

        private ConnectorResultTypedTags resultFoundTypedTags;
        public ConnectorResultTypedTags ResultFoundTypedTags
        {
            get
            {
                lock(connectorLocker)
                {
                    return resultFoundTypedTags;
                }
            }
            set
            {
                lock(connectorLocker)
                {
                    resultFoundTypedTags = value;
                }

                RaisePropertyChanged(nameof(ResultFoundTypedTags));
            }
        }

        private clsOntologyItem resultDeleteTypedTag;
        public clsOntologyItem ResultDeleteTypedTag
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultDeleteTypedTag;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultDeleteTypedTag = value;
                }
                RaisePropertyChanged(nameof(ResultDeleteTypedTag));
            }
        }

        private clsOntologyItem resultReorderTypedTag;
        public clsOntologyItem ResultReorderTypedTag
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultReorderTypedTag;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultReorderTypedTag = value;
                }
                RaisePropertyChanged(nameof(ResultReorderTypedTag));
            }
        }

        public async Task<ConnectorResultTypedTags> GetTags(clsOntologyItem oItemTagSource)
        {
            var result = new ConnectorResultTypedTags();

            result.Result = localConfig.Globals.LState_Success.Clone();

            var typedTagsTask = serviceAgentElastic.GetTypedTagsByTaggingSources(new List<clsOntologyItem> { oItemTagSource });
            typedTagsTask.Wait();

            result.Result = typedTagsTask.Result.Result;

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultFoundTypedTags = result;
                return ResultFoundTypedTags;
            }

            var typedTagsFactoryTask = typedTaggingFactory.CreateTypedTagsList(typedTagsTask.Result.TypedTagsToTaggingSources, typedTagsTask.Result.TypedTagsToTags);
            typedTagsFactoryTask.Wait();

            result.Result = typedTagsFactoryTask.Result.Result;

            result.TypedTags = typedTagsFactoryTask.Result.TypedTags;

            ResultFoundTypedTags = result;
            return ResultFoundTypedTags;
        }

        public async Task<ConnectorResultTypedTags> SaveTags(clsOntologyItem oItemTagSource, List<clsOntologyItem> tags, clsOntologyItem oItemUser, clsOntologyItem oItemGroup, bool sendToView)
        {
            var connectorResult = new ConnectorResultTypedTags
            {
                Result = localConfig.Globals.LState_Nothing.Clone(),
                TaggingSource = oItemTagSource,
                Tags = tags
            };

            var serviceResult = serviceAgentElastic.SaveTypedTags(oItemTagSource, tags, oItemUser, oItemGroup);
            serviceResult.Wait();

            connectorResult.Result = serviceResult.Result.Result;
            
            if (connectorResult.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultSaveTypedTags = connectorResult;
                return connectorResult;
            }

            var factoryResult = typedTaggingFactory.CreateTypedTagsList(serviceResult.Result.TypedTagsToTaggingSources, serviceResult.Result.TypedTagsToTags);
            factoryResult.Wait();

            var typedTags =
                factoryResult.Result.TypedTags.Where(
                    typedTagItem =>
                        typedTagItem.IdTagSource == oItemTagSource.GUID &&
                        tags.Any(tagItem => tagItem.GUID == typedTagItem.IdTag)).ToList();

            connectorResult.Result = factoryResult.Result.Result;
            connectorResult.TypedTags = typedTags;

            if (sendToView)
            {
                ResultSaveTypedTags = connectorResult;
            }
            else
            {

            }
            
            return connectorResult;
        }

        public async Task<clsOntologyItem> ReorderTypedTags(List<TypedTag> typedTags)
        {
            var resultTask = serviceAgentElastic.ReOrderTypedTags(typedTags);
            resultTask.Wait();

            var result = resultTask.Result;
            ResultReorderTypedTag = result;
            return result;
        }

        public async Task<clsOntologyItem> DeleteTag(clsOntologyItem oItemTypedTag)
        {
            var resultTask = serviceAgentElastic.DeleteTypedTag(oItemTypedTag);
            resultTask.Wait();

            ResultDeleteTypedTag = resultTask.Result;
            return resultTask.Result;
        }
 
        public TypedTaggingConnector(Globals globals)
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            typedTaggingFactory = new TypedTaggingFactory();
        }
    }

    public class ConnectorResultTypedTags
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem TaggingSource { get; set; }
        public List<clsOntologyItem> Tags { get; set; }
        public List<TypedTag> TypedTags { get; set; }
    }
}
