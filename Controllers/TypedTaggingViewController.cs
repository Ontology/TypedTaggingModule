﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Factories;
using TypedTaggingModule.Models;
using TypedTaggingModule.Services;
using TypedTaggingModule.Translations;
using System.ComponentModel;

namespace TypedTaggingModule.Controllers
{
    public class TypedTaggingViewController : TypedTaggingViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgentElastic serviceAgentElastic;

        private clsLocalConfig localConfig;

        private clsOntologyItem oItemTagSource;

        private TypedTaggingFactory typedTaggingFactory;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public TypedTaggingViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += TypedTaggingViewController_PropertyChanged;
        }

        private void TypedTaggingViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            if (e.PropertyName == nameof(DataText_TypedTag))
            {
                if (IsEnabled_TypedTagEnabled)
                {
                    var oItemTypedTag = new clsOntologyItem
                    {
                        GUID = SelectedTypedTag_Selected.TypedTag.IdTypedTag,
                        Name = DataText_TypedTag,
                        GUID_Parent = localConfig.OItem_class_typed_tag.GUID,
                        Type = localConfig.Globals.Type_Object
                    };

                    var result = serviceAgentElastic.SaveTypedTagName(oItemTypedTag);

                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        IsEnabled_TypedTagEnabled = false;
                        DataText_TypedTag = SelectedTypedTag_Selected.TypedTag.NameTypedTag;
                    }
                    else
                    {
                        SelectedTypedTag_Selected.TypedTag.NameTypedTag = oItemTypedTag.Name;
                        ChangedTypedTag_TypedTag = new ChangedTypedTag
                        {
                            Uid = SelectedTypedTag_Selected.uid,
                            PropertyName = nameof(TypedTag.NameTypedTag),
                            TypedTag = SelectedTypedTag_Selected.TypedTag
                        };
                    }
                    return;
                }
                else
                {

                }
            }

            if (e.PropertyName == nameof(Long_OrderId))
            {
                if (IsEnabled_OrderId)
                {
                    var oItemTypedTag = new clsOntologyItem
                    {
                        GUID = SelectedTypedTag_Selected.TypedTag.IdTypedTag,
                        Name = DataText_TypedTag,
                        GUID_Parent = localConfig.OItem_class_typed_tag.GUID,
                        Type = localConfig.Globals.Type_Object
                    };

                    var oItemTagSource = new clsOntologyItem
                    {
                        GUID = SelectedTypedTag_Selected.TypedTag.IdTagSource,
                        Name = SelectedTypedTag_Selected.TypedTag.NameTagSource,
                        Type = localConfig.Globals.Type_Object
                    };

                    oItemTagSource = serviceAgentElastic.GetOItem(oItemTagSource.GUID, localConfig.Globals.Type_Object);

                    if (oItemTagSource == null) return;

                    var result = serviceAgentElastic.ChangeOrderId(oItemTypedTag, oItemTagSource, Long_OrderId);

                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        IsEnabled_OrderId = false;
                        Long_OrderId = SelectedTypedTag_Selected.TypedTag.OrderId;
                    }
                    else
                    {
                        SelectedTypedTag_Selected.TypedTag.OrderId = Long_OrderId;
                        ChangedTypedTag_TypedTag = new ChangedTypedTag
                        {
                            Uid = SelectedTypedTag_Selected.uid,
                            PropertyName = nameof(TypedTag.OrderId),
                            TypedTag = SelectedTypedTag_Selected.TypedTag
                        };
                    }

                    return;
                }



            }

            if (e.PropertyName == nameof(SelectedTypedTag_Selected))
            {
                if (SelectedTypedTag_Selected == null) return;

                if (SelectedTypedTag_Selected.PropertyName == nameof(TypedTag.NameTypedTag))
                {
                    var idTypedTag = SelectedTypedTag_Selected.TypedTag.IdTypedTag;
                    var typedTag = serviceAgentElastic.GetOItem(idTypedTag, localConfig.Globals.Type_Object);

                    if (typedTag == null) return;

                    DataText_TypedTag = typedTag.Name;
                    IsEnabled_TypedTagEnabled = true;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem>
                        {
                            typedTag
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);

                }
                else if (SelectedTypedTag_Selected.PropertyName == nameof(TypedTag.OrderId))
                {
                    Long_OrderId = SelectedTypedTag_Selected.TypedTag.OrderId;
                    IsEnabled_OrderId = true;
                }
                else if (SelectedTypedTag_Selected.PropertyName == nameof(TypedTag.NameTagSource))
                {
                    var idTypedTag = SelectedTypedTag_Selected.TypedTag.IdTagSource;
                    var tagSource = serviceAgentElastic.GetOItem(idTypedTag, localConfig.Globals.Type_Object);

                    if (tagSource == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem>
                        {
                            tagSource
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);

                }
                else if (SelectedTypedTag_Selected.PropertyName == nameof(TypedTag.NameTag))
                {
                    var idTypedTag = SelectedTypedTag_Selected.TypedTag.IdTag;
                    var tag = serviceAgentElastic.GetOItem(idTypedTag, localConfig.Globals.Type_Object);

                    if (tag == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem>
                        {
                            tag
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);

                }

                return;
            }

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            typedTaggingFactory = new TypedTaggingFactory();
            typedTaggingFactory.PropertyChanged += TypedTaggingFactory_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndpointType = EndpointType.Receiver,
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndpointType = EndpointType.Receiver,
            });

            IsToggled_Listen = true;
            IsToggled_ListenApplied = true;

            if (oItemTagSource == null && webSocketServiceAgent.ObjectArgument != null)
            {
                CheckSendOrParamObject(webSocketServiceAgent.ObjectArgument.Value.ToString());
            }

            Long_NextOrderId = 1;
            
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsEnabled_TypedTagEnabled = false;
                IsEnabled_OrderId = false;
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void TypedTaggingFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TypedTaggingFactory.ResultTypedTags))
            {
                if (typedTaggingFactory.ResultTypedTags.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    var resultTask = GridFactory.WriteKendoJson(typeof(TypedTag), typedTaggingFactory.ResultTypedTags.TypedTags.ToList<object>(), sessionFile);

                    resultTask.Wait();

                    if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        KendoDataSource_Grid = GridFactory.CreateKendoGridConfig(typeof(TypedTag), sessionFile);
                    }
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgentElastic.ServiceResultTagsByTaggingSources))
            {
                if (serviceAgentElastic.ServiceResultTagsByTaggingSources.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var result = typedTaggingFactory.CreateTypedTagsList(serviceAgentElastic.ServiceResultTagsByTaggingSources.TypedTagsToTaggingSources,
                        serviceAgentElastic.ServiceResultTagsByTaggingSources.TypedTagsToTags);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

       
       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "clicked.listen")
                {
                    IsToggled_Listen = !IsToggled_Listen;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.listenApplied")
                {
                    IsToggled_ListenApplied = !IsToggled_ListenApplied;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.reload")
                {
                    CheckSendOrParamObject();
                }
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "typedTagSelected")
                    {
                        
                        var selectedTypedTag = Newtonsoft.Json.JsonConvert.DeserializeObject<SelectedTypedTag>(changedItem.ViewItemValue.ToString());
                        if (selectedTypedTag.PropertyName == nameof(TypedTag.NameTypedTag))
                        {
                            IsEnabled_TypedTagEnabled = false;
                        }
                        else if (selectedTypedTag.PropertyName == nameof(TypedTag.OrderId))
                        {
                            IsEnabled_OrderId = false;
                        }
                        
                        SelectedTypedTag_Selected = selectedTypedTag;
                    }
                    
                });

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectId = webSocketServiceAgent.ObjectArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(objectId)) return;
                if (!IsSuccessful_Login) return;
                if (!IsToggled_Listen) return;

                if ((oItemTagSource != null && objectId != oItemTagSource.GUID) || oItemTagSource == null)
                {

                    CheckSendOrParamObject(objectId);
                }
                
            }



        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            var oItemMessage = message.OItems.LastOrDefault();
            if (oItemMessage == null) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oItem = message.OItems.FirstOrDefault();

                if (oItem == null) return;
                if ((oItemTagSource != null && oItemMessage.GUID != oItemTagSource.GUID) || oItemTagSource == null)
                {
                    CheckSendOrParamObject(oItemMessage.GUID);
                }
            }
            else if (message.ChannelId == Channels.AppliedObjects && IsToggled_ListenApplied)
            {
                var objectItems = message.OItems;

                if (oItemTagSource == null) return;

                var nextOrderId = Long_NextOrderId;
                objectItems.ForEach(objectItem =>
                {
                    objectItem.Val_Long = nextOrderId;
                    nextOrderId++;
                });

                Long_NextOrderId = nextOrderId;

                var resultTask = serviceAgentElastic.SaveTypedTags(oItemTagSource, 
                    objectItems,
                    webSocketServiceAgent.oItemUser,
                    webSocketServiceAgent.oItemGroup);
                resultTask.Wait();

                if (resultTask.Result.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    ChangedTypedTag_TypedTagAdd = resultTask.Result.TypedTagsToTags.Select(typedTag => new TypedTag
                            {
                                IdTag = resultTask.Result.TypedTagsToTags.First().ID_Other,
                                NameTag = resultTask.Result.TypedTagsToTags.First().Name_Other,
                                IdTagParent = resultTask.Result.TypedTagsToTags.First().ID_Parent_Other,
                                NameTagParent = resultTask.Result.TypedTagsToTags.First().Name_Parent_Other,
                                IdTypedTag = resultTask.Result.TypedTagsToTaggingSources.First().ID_Object,
                                NameTypedTag = resultTask.Result.TypedTagsToTaggingSources.First().Name_Object,
                                IdTagSource = resultTask.Result.TypedTagsToTaggingSources.First().ID_Other,
                                NameTagSource = resultTask.Result.TypedTagsToTaggingSources.First().Name_Other,
                                OrderId = resultTask.Result.TypedTagsToTaggingSources.First().OrderID.Value,
                                TagType = resultTask.Result.TypedTagsToTags.First().Ontology
                            }).ToList();
                    
                }
                
            }
                
        }

        private void CheckSendOrParamObject(string idItem = null)
        {
            IsEnabled_TypedTagEnabled = false;
            DataText_TypedTag = "";
            if (!string.IsNullOrEmpty(idItem))
            {
                var paramObject = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);
                oItemTagSource = paramObject;
                if (paramObject == null) return;
            }
            
            var paramClass = serviceAgentElastic.GetOItem(oItemTagSource.GUID_Parent, localConfig.Globals.Type_Class);

            
            Text_View = oItemTagSource.Name + " (" + paramClass.Name + ")";

            GetTypedTags();
        }

        private void GetTypedTags()
        {
            if (oItemTagSource == null) return;
            IsToggled_Listen = false;
            var result = serviceAgentElastic.GetTypedTagsByTaggingSources(new List<clsOntologyItem>
            {
                oItemTagSource
            });
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
