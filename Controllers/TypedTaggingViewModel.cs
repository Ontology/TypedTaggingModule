﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace TypedTaggingModule.Controllers
{
    public class TypedTaggingViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(nameof(IsToggled_Listen));

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(nameof(Text_View));

            }
        }

        private Dictionary<string, object> kendoDatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public Dictionary<string, object> KendoDataSource_Grid
        {
            get { return kendoDatasource_Grid; }
            set
            {
                if (kendoDatasource_Grid == value) return;

                kendoDatasource_Grid = value;

                RaisePropertyChanged(nameof(KendoDataSource_Grid));

            }
        }

        private SelectedTypedTag selectedtypedtag_Selected;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "typedTagSelected", ViewItemType = ViewItemType.Other)]
		public SelectedTypedTag SelectedTypedTag_Selected
        {
            get { return selectedtypedtag_Selected; }
            set
            {

                selectedtypedtag_Selected = value;

                RaisePropertyChanged(nameof(SelectedTypedTag_Selected));

            }
        }

        private bool isenabled_TypedTagEnabled;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpTypedTag", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_TypedTagEnabled
        {
            get { return isenabled_TypedTagEnabled; }
            set
            {

                isenabled_TypedTagEnabled = value;

                RaisePropertyChanged(nameof(IsEnabled_TypedTagEnabled));

            }
        }

        private string datatext_TypedTag;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpTypedTag", ViewItemType = ViewItemType.Content)]
		public string DataText_TypedTag
        {
            get { return datatext_TypedTag; }
            set
            {
                if (datatext_TypedTag == value) return;

                datatext_TypedTag = value;

                RaisePropertyChanged(nameof(DataText_TypedTag));

            }
        }

        private ChangedTypedTag changedtypedtag_TypedTag;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "changedTypedTag", ViewItemType = ViewItemType.Other)]
		public ChangedTypedTag ChangedTypedTag_TypedTag
        {
            get { return changedtypedtag_TypedTag; }
            set
            {
                if (changedtypedtag_TypedTag == value) return;

                changedtypedtag_TypedTag = value;

                RaisePropertyChanged(nameof(ChangedTypedTag_TypedTag));

            }
        }

        private bool isToggled_ListenApplied;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "listenApplied", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_ListenApplied
        {
            get { return isToggled_ListenApplied; }
            set
            {

                isToggled_ListenApplied = value;

                RaisePropertyChanged(nameof(IsToggled_ListenApplied));

            }
        }

        private List<TypedTag> changedtypedtag_TypedTagAdd;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "addedTypedTag", ViewItemType = ViewItemType.Other)]
		public List<TypedTag> ChangedTypedTag_TypedTagAdd
        {
            get { return changedtypedtag_TypedTagAdd; }
            set
            {
                if (changedtypedtag_TypedTagAdd == value) return;

                changedtypedtag_TypedTagAdd = value;

                RaisePropertyChanged(nameof(ChangedTypedTag_TypedTagAdd));

            }
        }

        private bool isenabled_OrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "orderId", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_OrderId
        {
            get { return isenabled_OrderId; }
            set
            {

                isenabled_OrderId = value;

                RaisePropertyChanged(nameof(IsEnabled_OrderId));

            }
        }

        private long long_OrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "orderId", ViewItemType = ViewItemType.Content)]
		public long Long_OrderId
        {
            get { return long_OrderId; }
            set
            {
                if (long_OrderId == value) return;

                long_OrderId = value;

                RaisePropertyChanged(nameof(Long_OrderId));

            }
        }

        private long long_NextOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "nextOrderId", ViewItemType = ViewItemType.Content)]
        public long Long_NextOrderId
        {
            get { return long_NextOrderId; }
            set
            {
                if (long_NextOrderId == value) return;

                long_NextOrderId = value;

                RaisePropertyChanged(nameof(Long_NextOrderId));

            }
        }

    }
}
