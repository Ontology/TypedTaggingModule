﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class SelectedTypedTag
    {
        public string PropertyName { get; set; }
        public string uid { get; set; }
        public TypedTag TypedTag { get; set; }
    }
}
