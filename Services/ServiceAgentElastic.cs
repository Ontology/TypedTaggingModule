﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace TypedTaggingModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object serviceLocker = new object();

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private ServiceResult serviceResultTagsByTaggingSources;
        public ServiceResult ServiceResultTagsByTaggingSources
        {
            get
            {
                lock(serviceLocker)
                {
                    return serviceResultTagsByTaggingSources;
                }
                
            }
            set
            {
                lock(serviceLocker)
                {
                    serviceResultTagsByTaggingSources = value;
                }

                RaisePropertyChanged(nameof(ServiceResultTagsByTaggingSources));
            }
        }

        private ServiceResult serviceResultSaveTags;
        public ServiceResult ServiceResultSaveTags
        {
            get
            {
                lock(serviceLocker)
                {
                    return serviceResultSaveTags;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultSaveTags = value;
                    RaisePropertyChanged(nameof(ServiceResultSaveTags));
                }
            }
        }

        private clsOntologyItem serviceResultDeleteTag;
        public clsOntologyItem ServiceResultDeleteTag
        {
            get
            {
                lock (serviceLocker)
                {
                    return serviceResultDeleteTag;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultDeleteTag = value;
                    RaisePropertyChanged(nameof(ServiceResultDeleteTag));
                }
            }
        }

        private clsOntologyItem serviceResultReorderTypedTags;
        public clsOntologyItem ServiceResultReorderTypedTags
        {
            get
            {
                lock (serviceLocker)
                {
                    return serviceResultReorderTypedTags;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultReorderTypedTags = value;
                    RaisePropertyChanged(nameof(ServiceResultReorderTypedTags));
                }
            }
        }

        public async Task<ServiceResult> GetTypedTagsByTaggingSources(List<clsOntologyItem> taggingSources)
        {
            var serviceResult = new ServiceResult
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchResult1 = GetTypedTagsToTaggingSource(taggingSources);

            if (searchResult1.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.TypedTagsToTaggingSources = searchResult1.ResultList;

            var searchResult2 = GetTypedTagsToTags(serviceResult.TypedTagsToTaggingSources);

            if (searchResult2.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.TypedTagsToTags = searchResult2.ResultList;

            ServiceResultTagsByTaggingSources = serviceResult;
            return serviceResult;
        }

        private SearchResult<clsObjectRel> GetTypedTagsToTaggingSource(List<clsOntologyItem> taggingSources)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchTypedTags = taggingSources.Select(taggingSource => new clsObjectRel
            {
                ID_Other = taggingSource.GUID,
                ID_RelationType = localConfig.OItem_relationtype_is_tagging.GUID
            }).ToList();

            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            if (searchTypedTags.Any())
            {
                var result = dbReader.GetDataObjectRel(searchTypedTags);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = localConfig.Globals.LState_Error.Clone();
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
                return searchResult;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
                return searchResult;
            }
        }

        private SearchResult<clsObjectRel> GetTypedTagsToTags(List<clsObjectRel> typedTagsToTaggingSources)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchTags = typedTagsToTaggingSources.Select(typedTag => new clsObjectRel
            {
                ID_Object = typedTag.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging_tag.GUID
            }).ToList();

            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            if (searchTags.Any())
            {
                var result = dbReader.GetDataObjectRel(searchTags);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = localConfig.Globals.LState_Error.Clone();
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
                return searchResult;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
                return searchResult;
            }
        }

        public async Task<clsOntologyItem> DeleteTypedTag(clsOntologyItem oItemTypedTag)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var dbDeletor = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderLeftRight = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderRightLeft = new OntologyModDBConnector(localConfig.Globals);

            var searchLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemTypedTag.GUID
                }
            };

            result = dbReaderLeftRight.GetDataObjectRel(searchLeftRight);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ServiceResultDeleteTag = result;
                return result;
            }

            var searchRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemTypedTag.GUID
                }
            };

            result = dbReaderRightLeft.GetDataObjectRel(searchRightLeft);

            var deleteTypedTag = true;

            if (dbReaderRightLeft.ObjectRels.Any())
            {
                deleteTypedTag = false;
            }

            dbReaderLeftRight.ObjectRels.ForEach(objRel =>
            {
                if (objRel.ID_Parent_Other == localConfig.OItem_class_user.GUID && 
                    objRel.ID_RelationType == localConfig.OItem_relationtype_belongs_to.GUID)
                {
                    return;
                }

                if (objRel.ID_Parent_Other == localConfig.OItem_class_group.GUID &&
                    objRel.ID_RelationType == localConfig.OItem_relationtype_belongs_to.GUID)
                {
                    return;
                }

                if (objRel.ID_RelationType == localConfig.OItem_relationtype_belonging_tag.GUID)
                {
                    return;
                }

                if (objRel.ID_RelationType == localConfig.OItem_relationtype_is_tagging.GUID)

                    return;

                deleteTypedTag = false;
            });

            if (deleteTypedTag)
            {
                result = dbDeletor.DelObjectRels(dbReaderLeftRight.ObjectRels);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ServiceResultDeleteTag = result;
                    return result;
                }

                result = dbDeletor.DelObjects(new List<clsOntologyItem>
                {
                    oItemTypedTag
                });
            }

            ServiceResultDeleteTag = result;
            return result;
        }

        public async Task<clsOntologyItem> ReOrderTypedTags(List<TypedTag> typedTags)
        {
            long orderId = 1;
            var rels = typedTags.Select(typedTag =>
            {
                var relation = relationConfig.Rel_ObjectRelation(new clsOntologyItem { GUID = typedTag.IdTypedTag, Name = typedTag.NameTypedTag, GUID_Parent = localConfig.OItem_class_typed_tag.GUID, Type = localConfig.Globals.Type_Object },
                    new clsOntologyItem { GUID = typedTag.IdTagSource, Name = typedTag.NameTagSource, GUID_Parent = typedTag.IdParentTagSource, Type = localConfig.Globals.Type_Object },
                    localConfig.OItem_relationtype_is_tagging,
                    orderId: orderId);
                orderId++;
                return relation;
            }).ToList();

            var dbWriter = new OntologyModDBConnector(localConfig.Globals);

            var result = dbWriter.SaveObjRel(rels);
            ServiceResultReorderTypedTags = result;
            return result;
        }

        public async Task<ServiceResult> SaveTypedTags(clsOntologyItem oItemTagSource, List<clsOntologyItem> tags, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {
            var result = localConfig.Globals.LState_Success.Clone();


            var typedTagsResult = GetTypedTagsByTaggingSources(new List<clsOntologyItem> { oItemTagSource });
            typedTagsResult.Wait();

            if (typedTagsResult.Result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                var resultItem = new ServiceResult
                {
                    Result = typedTagsResult.Result.Result,
                };
                return resultItem;
            }

            List<clsObjectRel> relTagSourceResult = new List<clsObjectRel>();
            List<clsObjectRel> relTagDestResult = new List<clsObjectRel>();

            var tagsToInsert = (from tag in tags
                                join tagPresent in ServiceResultTagsByTaggingSources.TypedTagsToTags on tag.GUID equals tagPresent.ID_Other into tagsPresent
                                from tagPresent in tagsPresent.DefaultIfEmpty()
                                where tagPresent == null
                                select tag).ToList();


            if (!tagsToInsert.Any())
            {

                ServiceResultSaveTags = new ServiceResult
                {
                    Result = result,
                    TypedTagsToTaggingSources = ServiceResultTagsByTaggingSources.TypedTagsToTaggingSources,
                    TypedTagsToTags = ServiceResultTagsByTaggingSources.TypedTagsToTags
                };
                return ServiceResultSaveTags;
            }

            transaction.ClearItems();

            foreach (var tag in tagsToInsert)
            {
                var typedTag = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = tag.Name,
                    GUID_Parent = localConfig.OItem_class_typed_tag.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                result = transaction.do_Transaction(typedTag);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                var relTagSource = relationConfig.Rel_ObjectRelation(typedTag, oItemTagSource, localConfig.OItem_relationtype_is_tagging, orderId: tag.Val_Long.Value);

                result = transaction.do_Transaction(relTagSource);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                relTagSource.Name_Object = typedTag.Name;
                relTagSource.Name_Other = oItemTagSource.Name;

                var relTagDest = relationConfig.Rel_ObjectRelation(typedTag, tag, localConfig.OItem_relationtype_belonging_tag);

                result = transaction.do_Transaction(relTagDest);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                relTagDest.Name_Other = tag.Name;

                var rel = relationConfig.Rel_ObjectRelation(typedTag, oItemUser, localConfig.OItem_relationtype_belongs_to);

                result = transaction.do_Transaction(rel);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                rel = relationConfig.Rel_ObjectRelation(typedTag, oItemGroup, localConfig.OItem_relationtype_belongs_to);

                result = transaction.do_Transaction(rel);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                relTagSourceResult.Add(relTagSource);
                relTagDestResult.Add(relTagDest);
            }

            var resultItem1 = new ServiceResult
            {
                Result = result,
                TypedTagsToTaggingSources = relTagSourceResult,
                TypedTagsToTags = relTagDestResult
            };

            ServiceResultSaveTags = resultItem1;
            return resultItem1;

        }

        public clsOntologyItem SaveTypedTagName(clsOntologyItem oItemTypedTag)
        {
            transaction.ClearItems();

            var result = transaction.do_Transaction(oItemTypedTag);

            return result;
        }

        public clsOntologyItem ChangeOrderId(clsOntologyItem oItemTypedTag, clsOntologyItem oItemTagSource, long orderId)
        {
            transaction.ClearItems();

            var rel = relationConfig.Rel_ObjectRelation(oItemTypedTag, oItemTagSource, localConfig.OItem_relationtype_is_tagging, orderId: orderId);

            var result = transaction.do_Transaction(rel);

            return result;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);

        }

        public ServiceAgentElastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        public ServiceAgentElastic(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }
        }

        private void Initialize()
        {
            transaction = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }

    }


    public class SearchResult<TType>
    {
        public clsOntologyItem Result { get; set; }
        public List<TType> ResultList { get; set; }
    }

    public class ServiceResult
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> TypedTagsToTaggingSources { get; set; }
        public List<clsObjectRel> TypedTagsToTags { get; set; }
    }
}
